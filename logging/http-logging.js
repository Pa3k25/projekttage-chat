const { logger } = require('./winston-logger');
const crypto = require('crypto');

function logRequest(req, res, next) {
  // um den Request eindeutig zu identifizieren (auch im Logging)
  req._id = crypto.randomUUID();
  // ;${formatDate(new Date())}
  logger.debug(`${req._id} >>> ${req.method}; ${req.url}`);

  // Wichtig sich auf dieses Ereignis zu hängen, denn
  // sonst würde man keine response-Logs für Static-Files bekommen
  res.on('finish', () => {
    logger.debug(`${req._id} >>>> ${res.statusCode}; ${req.url}`);
  });

  next();
}

module.exports.logRequest = logRequest;
