// taken from
// https://betterstack.com/community/guides/logging/how-to-install-setup-and-use-winston-and-morgan-to-log-node-js-applications/
// Winston SysLog-Levels
// {
//   emerg: 0,
//   alert: 1,
//   crit: 2,
//   error: 3,
//   warning: 4,
//   notice: 5,
//   info: 6,
//   debug: 7
// PROVIDE VALUES AS STRINGS!!!

const winston = require('winston');
const { createLogger, format } = winston;
const { combine, timestamp, colorize, align, printf } = format;


const logger = createLogger({
  levels: winston.config.syslog.levels,
  format: combine(
    colorize({ all: true }),
    timestamp({
      format: 'YYYY-MM-DD HH:mm:ss.SSS', // 2022-01-25 03:23:10.350 PM
    }),
    align(),
    printf((info) => `[${info.timestamp}] ${info.level}: ${info.message}`),
  ),

  transports: [new winston.transports.Console()],
});

// to change the logging dynamically
function setGlobalLogLevel(logLevel) {
  logger.level = logLevel || 'debug';
}

module.exports.setGlobalLogLevel = setGlobalLogLevel;
module.exports.logger = logger;
