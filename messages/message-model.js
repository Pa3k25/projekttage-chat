const mongoose = require('mongoose');
const { User } = require('../users/user-model');

const mongooseMessageSchema = new mongoose.Schema({
  sender: {
    type: String,
    required: [true, 'sender is missing'],
  },
  reciever: {
    type: String,
    required: [true, 'reciever is missing'],
  },
  timeSent: {
    type: Date,
    // required: [true, 'time is missing'],
  },
  content: {
    type: String,
    trim: true,
    required: [true, 'content is missing'],
  }
});

const MessageValidationSchema = {
    sender: {
        type: "string",
        required: [true, 'sender is missing'],
      },
      reciever: {
        type: "string",
        required: [true, 'reciever is missing'],
      },
      timeSent: {
        type: "object",
        required: [true, 'time is missing'],
      },
      content: {
        type: "string",
        required: [true, 'content is missing'],
      }
};

const Message = mongoose.model('Message', mongooseMessageSchema);

module.exports.Message = Message;
module.exports.MessageValidationSchema = MessageValidationSchema;
