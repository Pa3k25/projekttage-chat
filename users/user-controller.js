const AppError = require('../errors/app-error');
const { User } = require('./user-model');

// async functions return promises (special objects)s
// all Execptions are caught by handleAsyncError
const controller = {
  create: async (payload) => {
    // do your business - logic checks here
    // handle errors here
    // send correct error-codes
    // in this version only 500 Code is sent

    // create a return mongoose-object
    return User.create(payload);
  },

  createMany: async (docs) => {
    return User.insertMany(docs);
  },

  dropCollection: async() => {
    return User.collection.drop();
  },

  // find() delivers all documents, if no argument is present
  getAll: () => User.find(),

  // query filter (as object) can be used to retrieve a filtered database-result
  getFiltered: (filter) => {
    return User.find(filter);
  },

  // check return-value:
  // if there is no user with this id, null is returned and 404 is the response
  getById: async (_id) => {
    let result = await User.findById(_id);
    if (!result) throw new AppError(404, `User with _id ${_id} not found`);

    return result;
  },

  // there is no 404 - deleted leads to 204 (in every case)
  deleteById: (_id) => {
    return User.deleteOne({ _id });
  },

  update: async (_id, payload) => {
    throw new Error('not implemented');
  },
};

module.exports = controller;
