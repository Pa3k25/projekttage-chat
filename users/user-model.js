const mongoose = require('mongoose');

const mongooseUserSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    index: true,
    required: [true, 'username is missing'],
    trim: true,
  },
  password: {
    type: String,
    required: [true, 'password is missing'],
  },
  firstname: {
    type: String,
    trim: true,
  },
  lastname: {
    type: String,
    trim: true,
    required: [true, 'lastname is missing'],
  },
  state: {
    type: String,
    enum: ['online', 'offline'],
    required: [true, 'state is missing'],
  },
  gender: {
    type: String,
    enum: ['male', 'female'],
    required: [true, 'gender is missing'],
  },
  age: {
    type: Number,
  },
  refreshToken: {
    type: String,
  },
});

const UserValidationSchema = {
  username: {
    type: 'string',
    required: true,
    validation: {
      validator: (un) => un.length >= 4 && un.length <= 50 && un.includes('@'),
      msg: 'username must have more than 4 and less than 50 characters. An @ must be included',
    },
  },
  password: {
    type: 'string',
  },
  firstname: {
    type: 'string',
  },
  lastname: {
    type: 'string',
  },
  state: {
    type: 'string',
    required: true,
    validation: {
      validator: (gender) => ['online', 'offline'].includes(gender),
    },
  },
  age: {
    type: 'number',
    required: true,
    validation: {
      validator: (age) => age >= 12 && age <= 120,
      msg: 'age only between 12 and 120 allowed.',
    },
  },
  gender: {
    type: 'string',
    required: true,
    validation: {
      validator: (gender) => ['male', 'female'].includes(gender),
    },
  },
  refreshToken: {
    type: 'string',
  },
};

const User = mongoose.model('User', mongooseUserSchema);

module.exports.User = User;
module.exports.UserValidationSchema = UserValidationSchema;
