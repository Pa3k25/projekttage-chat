import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainChatComponent } from './main-chat/main-chat.component';
import { MainChatService } from './main-chat/main-chat.service';

@NgModule({
  declarations: [
    AppComponent,
    MainChatComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
