import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MainChatService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    //withCredentials: true //this is required so that Angular returns the Cookies received from the server. The server sends cookies in Set-Cookie header. Without this, Angular will ignore the Set-Cookie header
  };
  constructor(private http: HttpClient) { }

  getAllDishes() {
    return this.http.get(`http://localhost:4000/api/messages`, this.httpOptions);
  }
}
