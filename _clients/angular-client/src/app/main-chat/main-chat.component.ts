import { Component, OnInit } from '@angular/core';
import { Message } from '../models/Message';
import { MainChatService } from './main-chat.service';

@Component({
  selector: 'app-main-chat',
  templateUrl: './main-chat.component.html',
  styleUrls: ['./main-chat.component.css']
})
export class MainChatComponent implements OnInit {
  dataSource: Message[] = [];

  ngOnInit(): void {
    
  }

  constructor(private mainChatService: MainChatService) { }

  getAllMessages() {
    this.mainChatService.getAllDishes().subscribe((data) => {
     this.dataSource = Object.assign([{} as Message], data);
     console.log(this.dataSource);
    });
  }

}
