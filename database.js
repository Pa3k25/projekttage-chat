const mongoose = require('mongoose');
const { CONFIG } = require('./app-configuration');
const { logger } = require('./logging/winston-logger');

async function connect() {
  let connectionString = CONFIG.MONGO_CONNECTION_STRING;
  let dbConnectTimeout = 3000;
  let recreateDatabase = CONFIG.RECREATE_DATABASE === 'true';

  if (recreateDatabase) {
    let connection = null;

    connection = await mongoose.createConnection(connectionString, {
      serverSelectionTimeoutMS: dbConnectTimeout,
    });
    await connection.dropDatabase();
    logger.warning('Current Database dropped !!');
  }

  await mongoose.connect(connectionString, {
    serverSelectionTimeoutMS: dbConnectTimeout,
  });

  logger.info(`Database connection to ${connectionString} established.`);
}

module.exports = { connect };
