/* ************************************************************************* */
/*                       SYP Template Full Stack - Backend                   */
/*                                                                           */
/*                                                                           */
/*  Achim Karasek                                                            */
/*  HTL Villach - Abteilung Informatik                                       */
/*  (c) 2022/23                                                              */
/* ********************'**************************************************** */

const express = require('express');

const dotenv = require('dotenv');
require('dotenv').config();

const cookieparser = require('cookie-parser');

const { connect } = require('./database');

const { logRequest } = require('./logging/http-logging');
const { logger, setGlobalLogLevel } = require('./logging/winston-logger');
const { errorHandler } = require('./errors/error-handler');
const { healthChecker } = require('./utils/health-checker.js');

const userRouter = require('./users/user-router');
const messageRouter = require('./messages/message-router');

const {
  CONFIG,
  loadConfiguration,
  logConfiguration,
} = require('./app-configuration');

const API_PREFIX = '/api';
const app = express();
dotenv.config();

function main() {
  // 1. Setup config and logging
  loadConfiguration();
  setGlobalLogLevel(CONFIG.LOG_LEVEL);
  logConfiguration();

  logger.info(`app started. current version 0.0.1`);

  app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
      res.header(
        "Access-Control-Allow-Methods",
        "PUT, POST, PATCH, DELETE, GET"
      );
      return res.status(200).json({});
    }
    next();
  })

  try {
    // 2. Database Connection
    logger.info(`Try to connect to "${CONFIG.MONGO_CONNECTION_STRING}"`);
    connect();

    // 3. Loading Middleware and Router
    logger.info(`Loading endpoints for app ...`);
    app.use(cookieparser());
    app.use(express.json());
    app.use(express.text());

    app.use(logRequest);

    // 3.1. Client-Template settings
    let clientFolder = __dirname + '/_clients/react-client/build';
    let opts = {};
    opts.index = 'index.html';

    // to serve all requests made from react-dev-proxy
    // comment in / out in DEV or Production mode .... issue !!
    app.get('*', (req, res, next) => {
      if (req.url.startsWith('/index.html'))
        res.sendFile('index.html', { root: clientFolder });
      else next();
    });

    if (clientFolder) app.use('/', express.static(clientFolder, opts));

    app.get(`${API_PREFIX}/unexpected-error-test`, (_req, res) => res.yyyy());

    // Load your router here
    app.get(`${API_PREFIX}/healthcheck`, healthChecker);
    app.use(`${API_PREFIX}/users`, userRouter);
    app.use(`${API_PREFIX}/messages`, messageRouter);


    // 3.4. Load the Errorhandler
    app.use(errorHandler);

    // 4. Start the WebServer
    logger.info(`Starting WebServer ...`);
    app.listen(CONFIG.PORT, CONFIG.HOSTNAME, () => {
      logger.info(
        `Chat Backend running on http://${CONFIG.HOSTNAME}:${CONFIG.PORT}`,
      );
    });
  } catch (err) {
    logger.error(`Startup problems:\n` + `App is not healthy!\n` + err);
  }
}

main();

module.exports = app;
