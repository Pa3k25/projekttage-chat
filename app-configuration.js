// Priority of Config:
// 1st > process environment variables (maybe loaded from .env file by dotenv)
// 2nd > cli arguments (named same as env var) - process.env["name"] is set to this value
// 3rd > hardcoded default value (optional) - process.env["name"] is set to this value

const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const argv = yargs(hideBin(process.argv)).argv;
const { logger } = require('./logging/winston-logger');

const CONFIG = {
  LOG_LEVEL: 'debug',
  HOSTNAME: '127.0.0.1',
  PORT: 4000,
  MONGO_CONNECTION_STRING: `mongodb://127.0.0.1/projekttage-chat`,
  RECREATE_DATABASE: false,
  DEMO_PLACEHOLDER: null,
  DEMO_DATA: false,
};

// in the end ALL config values are globally available via process.env
function loadConfiguration() {
  for (let configVar in CONFIG) {
    if (!process.env[configVar]) {
      if (argv[configVar]) {
        CONFIG[configVar] = argv[configVar];
      }
    } else {
      CONFIG[configVar] = process.env[configVar];
    }
  }
}

function logConfiguration() {
  logger.info('application configuration');
  logger.info('-------------------------');
  for (let configVar in CONFIG) {
    logger.info(`${configVar} = "${CONFIG[configVar]}"`);
  }
  logger.info('-------------------------');
}

module.exports = { CONFIG, loadConfiguration, logConfiguration };
