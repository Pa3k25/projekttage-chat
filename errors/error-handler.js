// responsibilty:
// get an error-object, try to find out which type of error has occured
// answer the http-request with the correct http status error-code
// last fallback is always 500 - internal server error
// differ between EXPECTED and UNEXPECTED Errors
function errorHandler(err, _req, res, next) {
  let defaultErrorCode = 500;

  if (err.name === 'TokenExpiredError' || err.name === 'JsonWebTokenError') {
    // bekannte jwt Token error mit dem standard access denied überschreiben 401
    err = { code: 401, message: 'access denied' };
  }

  // Attention
  // use custom app errors in your business - logic instead of this "hack"
  if (err.name === 'ValidationError') {
    err = { code: 400, message: err.message };
  }

  let errCode = err.code || defaultErrorCode;
  if (errCode < 400 || errCode > 599) {
    console.error(
      `wrong http error code. got ${errCode}, set to ${defaultErrorCode}`,
    );
    errCode = defaultErrorCode;
  }
  if (errCode === defaultErrorCode) {
    console.log('unexpected error occured');
    console.error(err);
  }

  // try to build a correct error object to answer the request
  let errToSend = {
    code: errCode,
    message: err?.message || 'internal server error',
  };

  res.status(errCode).json(errToSend);
}

function handleAsyncError(fn) {
  return (req, res, next) => {
    // if fn is an async function,
    // get the promise object and add a catch error handler
    // otherwise the default exceptionhandling will do the job
    try {
      const returnedPromise = fn(req, res, next);
      // next() must be called to ensure ALL middleware-functions are called
      if (returnedPromise) {
        returnedPromise.then(() => next()).catch((err) => next(err));
      } else {
        next();
      }
    } catch (err) {
      next(err);
    }
  };
}

module.exports.errorHandler = errorHandler;
module.exports.handleAsyncError = handleAsyncError;
