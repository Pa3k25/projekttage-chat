const mongoose = require('mongoose');

exports.healthChecker = async (req, res) => {
  try {
    if (mongoose.connection.readyState === 1)
      res.status(200).send('OK App healthy and connected to database');
    else res.status(500).send('DB Connection not ready');
  } catch (error) {
    res.status(500).send(`no database connection: ${error}`);
  }
};
